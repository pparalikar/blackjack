package blackjack;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Collections;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class DealerGUI extends JFrame implements ActionListener, WindowListener 
{

	/**
	 * Creates a swing awt based graphical interface for the game 
	 */
	
	
	/* Variable declaration */
	private static final long serialVersionUID = 1L;
	// Buttons for start and game start
	private JButton startButton, gameButton;
	// Text area for event display
	private JTextArea eventTextArea;
	// Port number input box
	private JTextField portTextField;
	// Dealer object
	private Dealer dealer;
	// Panel to display cards
	private JPanel midPan;
	//Panels for players
	private JPanel player1Pane,player2Pane,player3Pane,player4Pane,player5Pane,player6Pane;
	//Labels for players
	private JLabel player1Label, player2Label, player3Label, player4Label, player5Label, player6Label;
	
	/*
	 * GUI Constructor
	 */
	public DealerGUI(int port) {
		super("Dealer");
		dealer = null;
		
		/* Set the top panel */
		JPanel topPan = new JPanel();
		topPan.setBackground(Color.WHITE);
		topPan.add(new JLabel("Port Number: "));
		portTextField = new JTextField(" " + port);
		topPan.add(portTextField);
		
		/* Put the start game button */
		startButton = new JButton("START DEALER");
		startButton.addActionListener(this);
		topPan.add(startButton);
		add(topPan, BorderLayout.NORTH);
		
		/* Game room */
		midPan = new JPanel(new GridLayout(6, 1));
		Dimension d = new Dimension(700,400);
		midPan.setSize(d);

		player1Pane = new JPanel(new FlowLayout(FlowLayout.LEFT));
		player1Pane.setBackground(new Color(10,108,3));
		player1Label = new JLabel("");
		player1Label.setForeground(Color.WHITE);
		player1Pane.add(player1Label);
		
		player2Pane = new JPanel(new FlowLayout(FlowLayout.LEFT));
		player2Pane.setBackground(new Color(10,108,3));
		player2Label = new JLabel("");
		player2Label.setForeground(Color.WHITE);
		player2Pane.add(player2Label);
		
		player3Pane = new JPanel(new FlowLayout(FlowLayout.LEFT));
		player3Pane.setBackground(new Color(10,108,3));
		player3Label = new JLabel("");
		player3Label.setForeground(Color.WHITE);
		player3Pane.add(player3Label);
		
		player4Pane = new JPanel(new FlowLayout(FlowLayout.LEFT));
		player4Pane.setBackground(new Color(10,108,3));
		player4Label = new JLabel("");
		player4Label.setForeground(Color.WHITE);
		player4Pane.add(player4Label);
		
		player5Pane = new JPanel(new FlowLayout(FlowLayout.LEFT));
		player5Pane.setBackground(new Color(10,108,3));
		player5Label = new JLabel("");
		player5Label.setForeground(Color.WHITE);
		player5Pane.add(player5Label);
		
		player6Pane = new JPanel(new FlowLayout(FlowLayout.LEFT));
		player6Pane.setBackground(new Color(10,108,3));
		player6Label = new JLabel("");
		player6Label.setForeground(Color.WHITE);
		player6Pane.add(player6Label);
		
		JScrollPane jsp = new JScrollPane(player1Pane);
		jsp.setBorder(null);
		midPan.add(jsp);
		
		jsp = new JScrollPane(player2Pane);
		jsp.setBorder(null);
		midPan.add(jsp);
		
		jsp = new JScrollPane(player3Pane);
		jsp.setBorder(null);
		midPan.add(jsp);
		
		jsp = new JScrollPane(player4Pane);
		jsp.setBorder(null);
		midPan.add(jsp);
		
		jsp = new JScrollPane(player5Pane);
		jsp.setBorder(null);
		midPan.add(jsp);
		
		jsp = new JScrollPane(player6Pane);
		jsp.setBorder(null);
		midPan.add(jsp);
		midPan.setBackground(new Color(10,108,3));
		//JScrollPane jsp = new JScrollPane(midPan);
		add(midPan, BorderLayout.WEST);
		
		/* Event room */
		JPanel eastPan = new JPanel();
		eventTextArea = new JTextArea(60,20);
		//eventTextArea.setEditable(false);
		appendEvent("Event log.\n");
		eastPan.add(new JScrollPane(eventTextArea));
	    add(eastPan, BorderLayout.EAST);
		
	    JPanel bottomPan = new JPanel();
	    gameButton = new JButton("START GAME");
	    Dimension preferredSize = startButton.getPreferredSize();
	    preferredSize.setSize(preferredSize.getWidth()*2, preferredSize.getHeight()*2);
		gameButton.setPreferredSize(preferredSize);
		gameButton.setVisible(false);
	    gameButton.addActionListener(this);
	    bottomPan.add(gameButton);
	    add(bottomPan, BorderLayout.SOUTH);
	    
		/* For the close button */
	    getContentPane().setBackground(new Color(10,108,3));
		addWindowListener(this);
		setSize(1000, 600);
		setVisible(true);
	}

	/*
	 * Append an event to the event room
	 */
	void appendEvent(String eventMsg)
	{
		eventTextArea.append(eventMsg);
		eventTextArea.setCaretPosition(eventTextArea.getText().length() - 1);
	}
	
	/*
	 * Puts the dealt card on the UI
	 */
	 void cardSet(int playerId, int cardValue)
	 {
	 	switch(playerId+1)
	 	{
	 	case 1:
	 		player1Pane.add(new JLabel(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("Cards/"+cardValue+".png")).getScaledInstance(56, 70, Image.SCALE_DEFAULT))));
	 		break;
	 	case 2:
	 		player2Pane.add(new JLabel(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("Cards/"+cardValue+".png")).getScaledInstance(56, 70, Image.SCALE_DEFAULT))));
	 		break;
	 	case 3:
	 		player3Pane.add(new JLabel(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("Cards/"+cardValue+".png")).getScaledInstance(56, 70, Image.SCALE_DEFAULT))));
	 		break;
	 	case 4:
	 		player4Pane.add(new JLabel(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("Cards/"+cardValue+".png")).getScaledInstance(56, 70, Image.SCALE_DEFAULT))));
	 		break;
	 	case 5:
	 		player5Pane.add(new JLabel(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("Cards/"+cardValue+".png")).getScaledInstance(56, 70, Image.SCALE_DEFAULT))));
	 		break;
	 	case 6:
	 		player6Pane.add(new JLabel(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("Cards/"+cardValue+".png")).getScaledInstance(56, 70, Image.SCALE_DEFAULT))));
	 		break;
	 	}
	 	midPan.repaint();
		midPan.revalidate();
	 }
	
	/*
	 * Start the dealer on click of button
	 */
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		Object o = e.getSource();
		if(o == startButton)
		{
			/* If running already, stop the dealer */
			if(dealer != null)
			{
				dealer.stop();
				dealer = null;
				portTextField.setEditable(true);
				return;
			}
			/* Start the dealer */
			int port;
			try
			{
				port = Integer.parseInt(portTextField.getText().trim());
			}	
			catch(Exception ex)
			{
				appendEvent("Invalid port number");
				return;
			}
			/* Create a new Dealer */
			dealer = new Dealer(port, this);
			/* Start the dealer as a thread */
			new DealerRunning().start();
			/* Disable the start button and make port number uneditable*/
			startButton.setEnabled(false);
			portTextField.setEditable(false);
			gameButton.setVisible(true);
		}
		if(o == gameButton)
		{
			//cleanUp();
			gameButton.setEnabled(false);
			
			/* Stop accepting more clients */
			dealer.stopAcceptingClients();
			
			/* Shuffle the deck of cards */
			for(int i=1;i<=52;i++)
			{
				dealer.deck.add(i);
			}
			Collections.shuffle(dealer.deck);
			
			//Set player names
			switch(dealer.players.size())
			{
			case 6:
				player6Label.setText(dealer.players.get(5).playerName);
			case 5:
				player5Label.setText(dealer.players.get(4).playerName);
			case 4:
				player4Label.setText(dealer.players.get(3).playerName);
			case 3:
				player3Label.setText(dealer.players.get(2).playerName);
			case 2:
				player2Label.setText(dealer.players.get(1).playerName);
			case 1:
				player1Label.setText(dealer.players.get(0).playerName);
			}
			
			//Broadcast player list to all
			dealer.broadcastList();
						
			//For all the players connected, deal two cards
			dealer.initDeal();
			
			midPan.repaint();
			midPan.revalidate();
			dealer.nextPlayer = 0;
			dealer.gameEngine(0);
			dealer.nextPlayer++;
		}
	}

	/*
	 * Main method
	 */
	public static void main(String[] args) {
		/* Call the dealer with port 1500 (default) */
		new DealerGUI(1500);
	}
	
	/*
	 * Cleans up the game window to start new game
	 */
	public void cleanUp()
	{
		midPan.removeAll();
		player1Pane.removeAll();
		player1Label=new JLabel("");
		player1Label.setForeground(Color.WHITE);
		player1Pane.add(player1Label);
		
		player2Pane.removeAll();
		player2Label=new JLabel("");
		player2Label.setForeground(Color.WHITE);
		player2Pane.add(player2Label);
		
		player3Pane.removeAll();
		player3Label=new JLabel("");
		player3Label.setForeground(Color.WHITE);
		player3Pane.add(player3Label);
		
		player4Pane.removeAll();
		player4Label=new JLabel("");
		player4Label.setForeground(Color.WHITE);
		player4Pane.add(player4Label);
		
		player5Pane.removeAll();
		player5Label=new JLabel("");
		player5Label.setForeground(Color.WHITE);
		player5Pane.add(player5Label);
		
		player6Pane.removeAll();
		player6Label=new JLabel("");
		player6Label.setForeground(Color.WHITE);
		player6Pane.add(player6Label);
		gameButton.setEnabled(true);
		
		JScrollPane jsp = new JScrollPane(player1Pane);
		jsp.setBorder(null);
		midPan.add(jsp);
		
		jsp = new JScrollPane(player2Pane);
		jsp.setBorder(null);
		midPan.add(jsp);
		
		jsp = new JScrollPane(player3Pane);
		jsp.setBorder(null);
		midPan.add(jsp);
		
		jsp = new JScrollPane(player4Pane);
		jsp.setBorder(null);
		midPan.add(jsp);
		
		jsp = new JScrollPane(player5Pane);
		jsp.setBorder(null);
		midPan.add(jsp);
		
		jsp = new JScrollPane(player6Pane);
		jsp.setBorder(null);
		midPan.add(jsp);
		
		midPan.repaint();
		midPan.revalidate();
		
		dealer.players.clear();
	}
	
	/*
	 * A thread to run the dealer
	 */
	class DealerRunning extends Thread
	{
		public void run()
		{
			/* should execute until it fails */
			dealer.wakeup();
			/* the dealer failed */
			startButton.setEnabled(true);
			portTextField.setEditable(true);
			appendEvent("Dealer crashed\n");
			dealer = null;
		}
	}

	/*
	 * Closing the window
	 */
	@Override
	public void windowClosing(WindowEvent arg0) {
		/* If the dealer exists */
		if(dealer != null)
		{
			try
			{
				// ask the dealer to stop
				dealer.stop();
			}
			catch(Exception e)
			{
				
			}
			dealer = null;
		}
		dispose();
		System.exit(0);
	}
	
	/*
	 * Unused methods
	 */
	@Override
	public void windowActivated(WindowEvent arg0) {}

	@Override
	public void windowClosed(WindowEvent arg0) {}
	@Override
	public void windowDeactivated(WindowEvent arg0) {}

	@Override
	public void windowDeiconified(WindowEvent arg0) {}

	@Override
	public void windowIconified(WindowEvent arg0) {}

	@Override
	public void windowOpened(WindowEvent arg0) {}

}
