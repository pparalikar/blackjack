package blackjack;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Dealer 
{
	/*
	 * The Dealer class acts as a Server for the application.
	 * Multiple players (clients) will connect to the dealer
	 */
	
	/* Declaration of variables	 */
	
	// Unique ID's for each connection
	private static int _connId;
	// List of players connected
	public ArrayList<PlayerThread> players;
	// Decider connection
	public DeciderThread deciderThread; 
	// GUI for the dealer
	private DealerGUI dgui;
	// Port number for dealer to list1en on
	private int port;
	// To stop the dealer
	private boolean keepOn;
	// To stop more players from connecting
	private boolean keepAccepting;
	// Deck of shuffled cards
	public ArrayList<Integer> deck; 
	//volatile boolean ackFlag;
	//public volatile boolean gameOver;
	int nextPlayer;
	
	/* Constructor for the dealer */
	public Dealer(int port, DealerGUI dgui) {
		this.port = port;
		this.dgui = dgui;
		players = new ArrayList<PlayerThread>();
		deck = new ArrayList<Integer>();
		//this.ackFlag = false;
		//this.gameOver = false;
	}
	
	/*
	 * Start the Dealer process
	 */
	public void wakeup()
	{
		keepOn = true;
		keepAccepting = true;
		try
		{
			/* Create the pseudo server socket for decider to connect */
			ServerSocket pseudoSoc = new ServerSocket(1499);
			/* Wait for decider to connect */
			show("Waiting for decider on port 1499");
			/* Accept incoming connection request from decider */
			Socket decideSoc = pseudoSoc.accept();
			deciderThread = new DeciderThread(decideSoc);
			deciderThread.start();
			
			/* Create the main Server socket */
			ServerSocket servSoc = new ServerSocket(port);
			/* Continue to run until interrupted
			 * Wait on keepOn value
			 */
			
			while(keepOn)
			{
				while(keepAccepting)
				{
				show("Waiting for players on port "+ port);
					/* Accept incoming connection request from players */
					Socket connSoc = servSoc.accept();
					
					/* Stop if server is to be stopped */
					if(!keepAccepting)
					{
						break;
					}
					
					/* Set a new thread for the new connection */
					PlayerThread playerThread = new PlayerThread(connSoc);
					
					/* Add the connection to the players list*/
					players.add(playerThread);
					playerThread.start();
					
					if(players.size()==6)
					{
						keepAccepting = false;
					}
				}
				if(!keepOn)
				{
					break;
				}
			}
			
			/* Stop the Server */
			try
			{
				servSoc.close();
				pseudoSoc.close();
				/* Close connections to all the players */
				for(int i=0; i<players.size(); i++)
				{
					PlayerThread playerThread = players.get(i);
					try
					{
						playerThread.sInput.close();
						playerThread.sOutput.close();
						playerThread.connSoc.close();
						deciderThread.sInput.close();
						deciderThread.sOutput.close();
						deciderThread.connSoc.close();
					}
					catch(IOException ioe)
					{
						/* No need to handle */
					}
				}
			}
			catch(Exception ioe)
			{
				/* Do not handle as this means server has already stopped */
			}
		}
		catch(IOException ioe)
		{
			show("Unable to start dealer process");
		}
	}
	
	/*
	 * Stop the dealer
	 */
	@SuppressWarnings("resource")
	protected void stop()
	{
		keepAccepting = false;
		keepOn = false;
		/* Try connecting to self as a client */
		try
		{
			new Socket("localhost", port);
		}
		catch(Exception e)
		{
			/* Dealer has stopped */
		}
	}
	
	/*
	 * Display an event on the console
	 */
	private void show(String msg)
	{
		dgui.appendEvent(msg+"\n");
	}
	
	/*
	 * Directed broadcast a message to all client
	 */
	private synchronized void broadcast(GameObject gameObject)
	{
		/* Remove client if write fails and client has disconnected */
		for(int i=0;i<players.size();i++)
		{
			PlayerThread playerThread = players.get(i);
			if(!playerThread.writeObject(gameObject))
			{
				players.remove(i);
				show("Disconnected player "+ playerThread.playerName+" removed from list");
			}
		}
	}
	
	/*
	 * Directed broadcast a message to all client
	 */
	private synchronized void unicast(GameObject gameObject, int requestor)
	{
		/* Remove client if write fails and client has disconnected */
		PlayerThread playerThread = players.get(requestor);
		if(!playerThread.writeObject(gameObject))
		{
			players.remove(requestor);
			show("Disconnected player "+ playerThread.playerName+" removed from list");
		}
	}
	
	/*
	 * Remove player
	 */
	synchronized void remove(int id)
	{
		for(int i=0; i < players.size(); i++)
		{
			PlayerThread playerThread = players.get(i);
			if(playerThread.id == id)
			{
				players.remove(i);
				return;
			}
		}
	}
	
	/*
	 * Initial dealing
	 */
	public void initDeal() {
		// for all the players connected
		for(int i=0;i<players.size();i++){
			// First deal
			dealCard(i);
			
			// Second deal
			dealCard(i);
		}
	}
	
	/*
	 * Deal a card and tell everyone about it
	 */
	public void dealCard(int requestor)
	{
		if(players.get(requestor).cardsDealt.size()<=11 && !players.get(requestor).holdFlag)
		{
			GameObject gameObject = new GameObject(GameObject.REQUEST, deck.get(0), players.get(requestor).playerName);
			players.get(requestor).cardsDealt.add(deck.get(0));
			dgui.cardSet(requestor,deck.get(0));
			int value = deck.get(0)%13;             //13 cards of each suit
			//show(""+value);
			if(value<=10 && value>0)
				players.get(requestor).handValue = players.get(requestor).handValue + value;  
			else
				players.get(requestor).handValue = players.get(requestor).handValue + 10;
			deck.remove(0);
			//show("Hand value "+players.get(requestor).handValue);
			broadcast(gameObject);
		}
		else
		{
			GameObject gameObject = new GameObject(GameObject.STOPSEND, "",0);
			unicast(gameObject, requestor);
		}
		if(players.get(requestor).handValue>21)
		{
			GameObject gameObject = new GameObject(GameObject.STOPSEND, "You lost", 0);
			unicast(gameObject, requestor);
			players.get(requestor).holdFlag = true;
		}
		else if(players.get(requestor).handValue>=17)
		{
			GameObject gameObject = new GameObject(GameObject.BESTVAL, "", 0);
			unicast(gameObject, requestor);
			players.get(requestor).holdFlag = true;
		}
		
		int flag = 0;
		for(int i=0;i<players.size();i++)
		{
			if(!players.get(i).holdFlag) 
			{
				flag=1;
				break;
			}
		}
		if(flag==0)
		{
			/* Signal the decider */
			show("Decider called");
			sendDecider();
			//gameOver = true;
		}
	}
	
	/*
	 * Broadcasts the list of players connected to all the players
	 */
	public void broadcastList()
	{
		// fetch list of players 
		String[] playerList = new String[6];
		for(int i=0;i<players.size();i++)
		{
			playerList[i] = players.get(i).playerName;
		}
		
		GameObject gameObject = new GameObject(GameObject.LIST, playerList);
		broadcast(gameObject);
	}
	
	/*
	 * Stop accepting more clients as game has started
	 */
	public void stopAcceptingClients() {
		keepAccepting = false;
	}
	
	/*
	 * Send message to decider
	 */
	public void sendDecider()
	{
		if(players.size()>0)
		{
			ArrayList<Integer> scoreList = new ArrayList<Integer>(6);
			for(int i=0;i<players.size();i++)
			{
				scoreList.add(players.get(i).handValue);
			}
			
			GameObject gameObject = new GameObject(GameObject.DECIDER, "", 0);
			gameObject.setScoreList(scoreList);
			deciderThread.writeObject(gameObject);
		}
		else
		{
			dgui.cleanUp();
			keepAccepting = true;
		}
	}
	
	/*
	 * Game driver method
	 */
	public void gameEngine(int nextPlayer)
	{
		try{
		if(players.size()>nextPlayer)
		{
			if(!players.get(nextPlayer).holdFlag)
			{
				GameObject gameObject = new GameObject(GameObject.TOKEN,"",nextPlayer);
				unicast(gameObject, nextPlayer);
				return;
			}
			else
			{
				while(players.get(nextPlayer).holdFlag)
				{
					this.nextPlayer++;
					nextPlayer = this.nextPlayer%players.size();
					if(players.size()>nextPlayer)
					{
						if(!players.get(nextPlayer).holdFlag)
						{
							GameObject gameObject = new GameObject(GameObject.TOKEN,"",nextPlayer);
							unicast(gameObject, nextPlayer);
							return;
						}
					}
					else break;
				}
			}
		}
		show("Decider called");
		sendDecider();
		}
		catch(Exception e)
		{
			
		}
	}
	
	public boolean holdall()
	{
		for(int i=0; i<players.size();i++)
		{
			if(!players.get(i).holdFlag) return false;
		}
		return true;
	}
	
	/*
	 * Inner class for player connection
	 */
	class PlayerThread extends Thread
	{
		/* Variable declarations */
		// socket for communication
		Socket connSoc;
		// input stream for fetching objects
		ObjectInputStream sInput;
		// output stream for sending objects
		ObjectOutputStream sOutput;
		// connection id
		int id;
		// player name
		String playerName;
		// Holder for GameObject received
		GameObject gameObject;
		// ip address
		String ip;
		// cards dealt list
		ArrayList<Integer> cardsDealt;
		// hold flag
		boolean holdFlag;
		// current value in hand
		int handValue;
		
		/* Constructor for the class */
		public PlayerThread(Socket connSoc)
		{
			id = ++_connId;
			holdFlag = false;
			handValue = 0;
			cardsDealt = new ArrayList<Integer>();
			this.connSoc = connSoc;
			ip = connSoc.getInetAddress().getHostAddress();
			
			/* Creation of i/o datastreams */
			try
			{
				sInput = new ObjectInputStream(connSoc.getInputStream());
				sOutput = new ObjectOutputStream(connSoc.getOutputStream());
				playerName = (String) sInput.readObject()+id;
				show(playerName + " just joined the game");
				GameObject gameObject = new GameObject(GameObject.START,playerName,0);
				this.writeObject(gameObject);
			}
			catch(IOException ioe)
			{
				show("Unable to setup the player connectivity");
				return;
			}
			catch(ClassNotFoundException cnfe)
			{
				/* No need to handle */
			}
		}
		
		/*
		 * Runner method
		 */
		public void run()
		{
			boolean keepOn = true;
			while(keepOn)
			{
				/* Read input as an object */
				try
				{
					gameObject = (GameObject)  sInput.readObject();
				}
				catch(IOException | ClassNotFoundException ioe)
				{
					remove(id);
					if(players.size()==0)
						sendDecider();
					else if(holdall())
						sendDecider();
					else
					{
						gameEngine(nextPlayer%players.size());
					}
					show(playerName + " left the game");
					break;
				}
				
				/* Message handling */
				switch(gameObject.getType())
				{
				case GameObject.REQUEST:
					/* Find player index from players list */
					int requestor = playerIndex(id);
					/* Deal a card and tell everyone */
					dealCard(requestor);
					gameEngine(nextPlayer%players.size());
					nextPlayer++;
					break;
				
				case GameObject.HOLD:
					int flag=0;
					/* Set the hold flag */
					holdFlag = true;
					/* If all clients have sent the hold messages, then signal the decider */
					for(int i=0;i<players.size();i++)
					{
						if(!players.get(i).holdFlag) 
						{
							flag=1;
							break;
						}
					}
					if(flag==0)
					{
						/* Signal the decider */
						show("call decider");
						sendDecider();
						//gameOver = true;
					}
					else
					{
						gameEngine(nextPlayer%players.size());
						nextPlayer++;
					}
					break;
				}
			}
			remove(id);
			close();
		}
		
		/*
		 * Find out the player index from the players list
		 */
		private int playerIndex(int id)
		{
			for(int i=0;i<players.size();i++)
			{
				if(players.get(i).id == id)
					return i;
			}
			return -1;
		}
		
		/*
		 * Write an object to the output stream
		 */
		private boolean writeObject(GameObject gameObject)
		{
			/* If client is still connected, send the object */
			if(!connSoc.isConnected())
			{
				close();
				return false;
			}
			
			/* Write object to the stream */
			try
			{
				sOutput.writeObject(gameObject);
			}
			catch(IOException ioe)
			{
				show("Unable to send message to the player "+this.playerName);
			}
			return true;
		}
		
		/*
		 * Close all the streams and socket connection
		 */
		private void close()
		{
			try
			{
				if(sOutput!= null)
					sOutput.close();
				if(sInput!= null)
					sInput.close();
				if(connSoc!= null)
					connSoc.close();
			}
			catch(Exception e){}
		}
	}
	
	class DeciderThread extends Thread
	{
		/* Variable declarations */
		// socket for communication
		Socket connSoc;
		// input stream for fetching objects
		ObjectInputStream sInput;
		// output stream for sending objects
		ObjectOutputStream sOutput;
		// Holder for GameObject received
		GameObject gameObject;
		// ip address
		String ip;
		
		/* Constructor for the class */
		public DeciderThread(Socket connSoc)
		{
			this.connSoc = connSoc;
			ip = connSoc.getInetAddress().getHostAddress();
			
			/* Creation of i/o datastreams */
			try
			{
				sInput = new ObjectInputStream(connSoc.getInputStream());
				sOutput = new ObjectOutputStream(connSoc.getOutputStream());
			}
			catch(IOException ioe)
			{
				show("Unable to setup the decider connectivity");
				return;
			}
		}
		
		/*
		 * Runner method
		 */
		public void run()
		{
			boolean keepOn = true;
			while(keepOn)
			{
				/* Read input as an object */
				try
				{
					gameObject = (GameObject)  sInput.readObject();
				}
				catch(IOException | ClassNotFoundException ioe)
				{
					show("Decider left the game");
					break;
				}
				
				/* Message handling */
				switch(gameObject.getType())
				{
				case GameObject.RESULT:
					/* Fetch the result from decider and broadcast to all*/
					int winnerIndex = gameObject.getWinnerIndex();
					if(winnerIndex>=0)
					{
						String winner = players.get(winnerIndex).playerName;
						show("Player "+winner+" won the game");
						gameObject.setRequestor(winner);
					}
					else
					{
						show("No winner for the game");
					}
					broadcast(gameObject);
					
					dgui.cleanUp();
					keepAccepting = true;
					break;
				}
			}
			close();
		}
		
		/*
		 * Write an object to the output stream
		 */
		private boolean writeObject(GameObject gameObject)
		{
			/* If client is still connected, send the object */
			if(!connSoc.isConnected())
			{
				close();
				return false;
			}
			
			/* Write object to the stream */
			try
			{
				sOutput.writeObject(gameObject);
			}
			catch(IOException ioe)
			{
				show("Unable to send message to the decider");
			}
			return true;
		}
		
		/*
		 * Close all the streams and socket connection
		 */
		private void close()
		{
			try
			{
				if(sOutput!= null)
					sOutput.close();
				if(sInput!= null)
					sInput.close();
				if(connSoc!= null)
					connSoc.close();
			}
			catch(Exception e){}
		}
	}
	
}
