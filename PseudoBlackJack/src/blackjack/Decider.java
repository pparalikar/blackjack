package blackjack;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.*;
import java.util.ArrayList;

public class Decider {
			/* Input Output */
			public ObjectInputStream sIn;
			/* To read from Socket */
			public ObjectOutputStream sOut;
			/*To write into socket*/
			private Socket socket;
			// Ip address of dealer
			private String dealer;
			private int port;

			Decider(String dealer){
				  this.dealer = dealer;
				  this.port = 1499;
				  }
			
			public boolean start() {
				
			try{
				socket = new Socket(dealer,port);
				}
				
			catch(Exception e){	
				return false;
				}
			
			try{
				sOut = new ObjectOutputStream(socket.getOutputStream());
				sIn = new ObjectInputStream(socket.getInputStream());
				
			}
			catch(Exception e){
				return false;
			}
			
			
			/* Thread creation to listen from dealer*/
			new ListenFromServer().start();
			System.out.println("Decider process started");
				return true;
				
			}
		
		
		void sendMessage(GameObject msg){
			
			try{
			if(msg.getType()==GameObject.RESULT){
				sOut.writeObject(msg);
				}
			}
			catch(IOException e){
				System.out.println(e);
				}
		
			}
			
		/*If something goes wrong then close all the connection*/
		private void disconnect(){
			
		try{
			if(sIn != null)
				sIn.close();
			}
		catch(Exception e){}
		/*nothing to be donw when sIn has NULL*/
		try{
			if(sOut !=null) sOut.close();
		}
			
		catch(Exception e) {}
			System.out.println("Decider stopped");
			System.exit(0);
			}
			
		/* a player that waits for message from the server */
		 
		class ListenFromServer extends Thread {
		@Override
		public void run() {
			ArrayList<Integer> scoreList = new ArrayList<Integer>();
		
			int curMax;
			int maxIndex;
			while(true){
				try{
				curMax = 0;
				maxIndex = -1;
				GameObject game = (GameObject)sIn.readObject();
				switch(game.getType())
				{
				case GameObject.DECIDER:
					// playerList = game.getPlayerList();
					scoreList = game.getScoreList();
					/* Deciding the winner */
					for(int i=0; i< scoreList.size() ; i++){
						if(scoreList.get(i)>curMax && scoreList.get(i) <=21)
						{
							System.out.println(scoreList.get(i));
							maxIndex = i;
							curMax = scoreList.get(i);
						}
					}
					GameObject game1 = new GameObject(GameObject.RESULT, "", -1);
					game1.setWinnerIndex(maxIndex);
					game1.setValue(curMax);
					sendMessage(game1);
					break;
					}
			  }	   
				
			catch(IOException e) {
				disconnect();
			}
				
			catch(ClassNotFoundException e2) {
				disconnect();
			}
			}
		  }
		}
		public static void main(String[] args) {
			Decider decider = new Decider("localhost");
			 if(!decider.start())
			 {
				 System.out.println("Decider not started");
				 return;
			 }
			
			
		}


}
