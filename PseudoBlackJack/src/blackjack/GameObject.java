package blackjack;

import java.io.Serializable;
import java.util.ArrayList;

public class GameObject implements Serializable {

	/*GameObject will be exchanged between dealer and client */
	
	private static final long serialVersionUID = 1L;
	
	static final int REQUEST = 1, HOLD = 2, START = 3,INFO = 4, LIST = 5, DECIDER=6, RESULT=7, STOPSEND=8, TOKEN=9, BESTVAL=10;
	private int type;
	private String message;
	private int value;
	private String requestor;
	private String[] playerList;
	private ArrayList<Integer> scoreList;
	private int winnerIndex;

	public ArrayList<Integer> getScoreList() {
		return scoreList;
	}

	public void setScoreList(ArrayList<Integer> scoreList) {
		this.scoreList = scoreList;
	}

	public int getWinnerIndex() {
		return winnerIndex;
	}

	public void setWinnerIndex(int winnerIndex) {
		this.winnerIndex = winnerIndex;
	}

	public GameObject(int type, String message, int value) {
		this.type = type;
		this.message = message;
		this.value = value;
	}
	
	public GameObject(int type, int value, String requestor) {
		this.type = type;
		this.requestor = requestor;
		this.value = value;
	}
	
	public GameObject(int type, String[] playerList) {
		this.type = type;
		this.playerList = playerList;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int getType() {
		return type;
	}
	
	public void setType(int type) {
		this.type = type;
	}
	
	public String getRequestor() {
		return requestor;
	}

	public void setRequestor(String requestor) {
		this.requestor = requestor;
	}

	public String[] getPlayerList() {
		return playerList;
	}

	public void setPlayerList(String[] playerList) {
		this.playerList = playerList;
	}
}