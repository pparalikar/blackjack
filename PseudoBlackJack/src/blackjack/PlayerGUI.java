package blackjack;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.net.URL;

import javax.swing.*;

public class PlayerGUI extends JFrame implements ActionListener, WindowListener {

	private static final long serialVersionUID = 1L;
	private JLabel label;
	Timer time; 
	JLabel timeLab;
	/* It will hold UserName */
	private JTextField tf;
	/*It will hold dealer's IP address and port number*/
	private JTextField tfServer, tfPort;
	/* Buttons on player screen */
	JButton login, Request, Fold, newGame;
	/* To display information on the screen */
    /* Default port number */
    private int ctr;  
    private Player player;
    String username;
    String server;
    int game;
    
    private JPanel midPanel, southPanel, northPanel;
    private JPanel player1, player2, player3, player4, player5, player6, result;
    private JLabel player1lab, player2lab, player3lab, player4lab, player5lab, player6lab;
    JLabel resultlab;
    JPanel serverAndPort;
    PlayerGUI(String host, int port, int game, String uname) {
    super ("black Jack ");
    this.game = game;
    this.username = uname;
    ctr = 2;
	northPanel = new JPanel(new GridLayout(4,1));
	/* the server name and port number */
	serverAndPort = new JPanel(new GridLayout(1,5,1,3));
	result = new JPanel();
	resultlab = new JLabel("Welcome to the game of luck");
	tfServer = new JTextField(host);
	tfServer.setEditable(true);
	tfPort = new JTextField("" + port);
	tfPort.setEditable(true);
	tfPort.setHorizontalAlignment(SwingConstants.RIGHT);

	serverAndPort.add(new JLabel("Server Address :"));
	serverAndPort.add(tfServer);
	serverAndPort.add(new JLabel("Port Number:"));
	serverAndPort.add(tfPort);
	serverAndPort.add(new JLabel(""));
	result.add(resultlab);
	
	/*Adding server and port field to GUI*/
	northPanel.add(serverAndPort);
	/* Label for name */
	
	label = new JLabel("Enter Your Name Below",SwingConstants.CENTER);
    northPanel.add(label);
    
	tf = new JTextField("User");
	tf.setForeground(Color.black);
	tf.setBackground(Color.white);
	northPanel.add(tf);
	northPanel.add(result);
	add(northPanel, BorderLayout.NORTH);
	
	login = new JButton("Login");
	login.addActionListener(this);
	newGame = new JButton("Play Again");
	newGame.addActionListener(this);
	newGame.setVisible(false);
	Request = new JButton("Request");
	Request.addActionListener(this);
	Request.setEnabled(false);
	/* login is required before logout */
	Fold = new JButton("Fold");
	Fold.addActionListener(this);
	Fold.setEnabled(false);
	
	midPanel = new JPanel(new GridLayout(6,1));
	player1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
	player1lab = new JLabel("");
	player1.setBackground(new Color(10,107,3));
	player1.add(player1lab);
	
	player2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
	player2lab = new JLabel("");
	player2.setBackground(new Color(10,107,3));
	player2.add(player2lab);
	
	player3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
	player3lab = new JLabel("");
	player3.setBackground(new Color(10,107,3));
	player3.add(player3lab);
	
	player4 = new JPanel(new FlowLayout(FlowLayout.LEFT));
	player4lab = new JLabel("");
	player4.setBackground(new Color(10,107,3));
	player4.add(player4lab);
	
	player5 = new JPanel(new FlowLayout(FlowLayout.LEFT));
	player5lab = new JLabel("");
	player5.setBackground(new Color(10,107,3));
	player5.add(player5lab);
	
	player6 = new JPanel(new FlowLayout(FlowLayout.LEFT));
	player6lab = new JLabel("");
	player6.setBackground(new Color(10,107,3));
	player6.add(player6lab);
	

	midPanel.add(player1, BorderLayout.WEST);
	midPanel.add(player2, BorderLayout.WEST);
	midPanel.add(player3, BorderLayout.WEST);
	midPanel.add(player4);
	midPanel.add(player5);
	midPanel.add(player6);
	midPanel.setBackground(new Color(10,107,3));

	add(midPanel, BorderLayout.WEST);
	southPanel = new JPanel();
	timeLab = new JLabel("");
	southPanel.add(login);
	southPanel.add(Request);
	southPanel.add(Fold);
	southPanel.add(newGame);
	southPanel.add(timeLab);
	add(southPanel, BorderLayout.SOUTH);
	
	setDefaultCloseOperation(EXIT_ON_CLOSE);
	setSize(1000,600);
	setVisible(true);
	getContentPane().setBackground(new Color(10,107,3));
	tf.requestFocus();
	if(game == 1)
	{
		tf = new JTextField(username);
		player = new Player(server, port, username, this);
		if(!player.start())
		 {
			 System.out.println("Player not started");
			 return;
		 }
		 login.setEnabled(false);
		 serverAndPort.setVisible(false);
		 tf.setEnabled(false);
		 northPanel.repaint();
		 northPanel.revalidate();
	}
}
	
 void connectionFailed(){
	 login.setEnabled(true);
	 Fold.setEnabled(false);
	 Request.setEnabled(false);
}
 public void actionPerformed(ActionEvent e){
	 Object o = e.getSource();
	 
	 if(o == Request){
		 time.stop();
		 timeLab.setText("");
		 if(ctr > 10)
			 Request.setEnabled(false);
		 else{
		 player.sendMessage(new GameObject(GameObject.REQUEST, "", 0));
	   	 ctr++;
	   	 }
		 Request.setEnabled(false);
		 Fold.setEnabled(false);
		 return;
	 }
	 if(o == Fold){
		 time.stop();
		 timeLab.setText("");
		 player.sendMessage(new GameObject(GameObject.HOLD, "", 0));
		 Request.setEnabled(false);
		 Fold.setEnabled(false);
		 return;
	 }
	 
	 if(o == login) {
		 
		username = tf.getText().trim();
		 if(username.length() == 0)
			 return;
		 
		 server = tfServer.getText().trim();
		 if(server.length() == 0)
			 return;
		 String portNumber = tfPort.getText().trim();
		 if(portNumber.length() == 0)
			 return;
		 int port = 0;
		 try {
			 port = Integer.parseInt(portNumber);
		 }
		 catch(Exception en){
			 return;
		 }
		 player = new Player(server, port, username, this);
	 
		 if(!player.start())
		 {
			 System.out.println("Player not started");
			 return;
		 }

		 login.setEnabled(false);
		 serverAndPort.setVisible(false);
		 tf.setEnabled(false);
		 northPanel.repaint();
		 northPanel.revalidate();

	 }
	 if(o == newGame)
	 {
		 new PlayerGUI("10.0.0.11", 1500, 1, username);
		 this.dispose();
		 
	 }
	 /* checking empty server name and port number*/
	 
	 
	}
 	
	public void append(GameObject game) {
		switch(game.getType()){
		case GameObject.REQUEST:
		Request.setEnabled(false);
		Fold.setEnabled(false);
		int val = game.getValue();
		String requestor = game.getRequestor();
		if(player1lab.getText().equalsIgnoreCase(requestor)){
			player1.add(new JLabel(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("Cards/"+val+".png")).getScaledInstance(56, 70, Image.SCALE_DEFAULT))));	
		}
		else if(player2lab.getText().equalsIgnoreCase(requestor)){
			player2.add(new JLabel(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("Cards/"+val+".png")).getScaledInstance(56, 70, Image.SCALE_DEFAULT))));	
		}
		else if(player3lab.getText().equalsIgnoreCase(requestor)){
			player3.add(new JLabel(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("Cards/"+val+".png")).getScaledInstance(56, 70, Image.SCALE_DEFAULT))));	
		}
		else if(player4lab.getText().equalsIgnoreCase(requestor)){
			player4.add(new JLabel(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("Cards/"+val+".png")).getScaledInstance(56, 70, Image.SCALE_DEFAULT))));	
		}
		else if(player5lab.getText().equalsIgnoreCase(requestor)){
			player5.add(new JLabel(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("Cards/"+val+".png")).getScaledInstance(56, 70, Image.SCALE_DEFAULT))));	
		}
		else if(player6lab.getText().equalsIgnoreCase(requestor)){
			player6.add(new JLabel(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("Cards/"+val+".png")).getScaledInstance(56, 70, Image.SCALE_DEFAULT))));	
		}
		midPanel.repaint();
		midPanel.revalidate();
		break;
		case GameObject.START :
			String playerName = game.getMessage();
			label.setText("Hi "+ playerName );
			northPanel.repaint();
			northPanel.revalidate();
			break;
		}
	}
	public void playerSetter(String[] playerlist) {
		switch(playerlist.length){
		case 6:
			  player6lab.setText(playerlist[5]);
		case 5:
			  player5lab.setText(playerlist[4]);
		case 4:
			  player4lab.setText(playerlist[3]);
		case 3:
			  player3lab.setText(playerlist[2]);
		case 2:
			  player2lab.setText(playerlist[1]);
		case 1:
			  player1lab.setText(playerlist[0]);
		}
		
		for(int i=0; i < playerlist.length; i++)
		{
			if(playerlist[i].equalsIgnoreCase(player.playerName))
			{
				switch(i+1){
				case 6:
					  player6.setBackground(Color.yellow);
					  break;
				case 5:
					player5.setLocation(player6.getLocation());
					player5.setBackground(Color.yellow);
					  break;
				case 4:
					player4.setLocation(player6.getLocation());
					player4.setBackground(Color.yellow);
					  break;
				case 3:
					player3.setLocation(player6.getLocation());
					player3.setBackground(Color.yellow);
					  break;
				case 2:
					player2.setLocation(player6.getLocation());
					player2.setBackground(Color.yellow);
					  break;
				case 1:
					player1.setLocation(player6.getLocation());
					player1.setBackground(Color.yellow);
					  break;
				}
				break;
			}
		}
	 midPanel.revalidate();
	 midPanel.repaint();
	}

	public static void main(String[] args) {
		new PlayerGUI("10.42.0.107", 1500, 0 ,"");
	}

	public void gameResult(String string) {
		newGame.setVisible(true);
		
		Request.setEnabled(false);
		Fold.setEnabled(false);
		login.setEnabled(false);
		URL url = getClass().getResource("Cards/sound.wav");
		AudioClip clip = Applet.newAudioClip(url);
		clip.play();
		JOptionPane.showMessageDialog(null, string);
		resultlab.setText(string);
		result.repaint();
		result.revalidate();
	}

	public void stopSend() {
		Request.setEnabled(false);
		Fold.setEnabled(false);
		resultlab.setText("You are out of the game !");
	}

	public void enableReq() {
		Request.setEnabled(true);
		Fold.setEnabled(true);
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		GameObject game = new GameObject(GameObject.HOLD, "", 0);
		player.sendMessage(game);
		dispose();
		System.exit(0);
	}

	@Override
	public void windowOpened(WindowEvent e) {}
	@Override
	public void windowClosed(WindowEvent e) {}
	@Override
	public void windowIconified(WindowEvent e) {}
	@Override
	public void windowDeiconified(WindowEvent e) {}
	@Override
	public void windowActivated(WindowEvent e) {}
	@Override
	public void windowDeactivated(WindowEvent e) {}
	
}
