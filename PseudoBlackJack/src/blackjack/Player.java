package blackjack;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.*;

import javax.swing.Timer;

public class Player {
			/* Input Output */
			public ObjectInputStream sIn;
			/* To read from Socket */
			public ObjectOutputStream sOut;
			/*To write into socket*/
			private Socket socket;

			private PlayerGUI pGUI;
			String dealer,playerName;
			
			
			private int port;
			/*Calling with GUI */
			Player(String dealer,int port,String playerName,PlayerGUI pg){
				  this.dealer = dealer;
				  this.port = port;
				  this.playerName = playerName;
				  pGUI = pg;
			}
			
			public boolean start() {
				
			try{
				socket = new Socket(dealer,port);
				}
				
			catch(Exception e){	
				return false;
				}
			
					
			try{
				sOut = new ObjectOutputStream(socket.getOutputStream());
				sIn = new ObjectInputStream(socket.getInputStream());
				
			}
			catch(Exception e){
				return false;
			}
			
			
			/* Thread creation to listen from dealer*/
			/*PlayerName is sent to server as string all other objects are sent as GameObject */
			
			new ListenFromServer().start();
			
			try{
				sOut.writeObject(playerName);
			}
			catch(IOException e){
				disconnect();
				return false;
					
			}
				return true;
				
			}
		
		
		void sendMessage(GameObject msg){
			
			try{
			if(msg.getType()==GameObject.REQUEST) {
				sOut.writeObject(msg);
				}
			else if(msg.getType()==GameObject.HOLD){
				pGUI.resultlab.setText("You have folded, wait for the decision");
				sOut.writeObject(msg);
			}
	
			}
			catch(IOException e){
			}
		}
			
		/*If something goes wrong then close all the connection*/
		private void disconnect(){
			
		try{
			if(sIn != null)
				sIn.close();
			}
		catch(Exception e){}
		/*nothing to be donw when sIn has NULL*/
		try{
			if(sOut !=null) sOut.close();
		}
			
		catch(Exception e) {}
			
			}		
		/* a player that waits for message from the server */
		class ListenFromServer extends Thread {
		@Override
		public void run() {
			int flag = 0;
			while(true){
				try{
				GameObject game = (GameObject)sIn.readObject();
				switch(game.getType())
				{
				case GameObject.LIST :
					String playerlist[] = game.getPlayerList();
					pGUI.playerSetter(playerlist);
					break;
				case GameObject.START :
					playerName = game.getMessage();
						
					pGUI.append(game);	
					break;
				case GameObject.REQUEST :
					pGUI.append(game);
					break;
				case GameObject.INFO :
					pGUI.append(game);
					break;
				case GameObject.RESULT :
					try{
					pGUI.time.stop();
					}catch(Exception e){}
					pGUI.timeLab.setText("");
					if(game.getWinnerIndex() < 0)
						pGUI.gameResult("No winner");
					else if(game.getRequestor().equalsIgnoreCase(playerName))
						pGUI.gameResult("You Won with hand value "+game.getValue()+" !");
					else
						pGUI.gameResult(game.getRequestor() + " Won the game with hand value "+game.getValue());
					flag = 1;
					break;
				case GameObject.STOPSEND :
					try{
						pGUI.time.stop();
						}catch(Exception e){}
					pGUI.timeLab.setText("");
					pGUI.stopSend();
					break;
				case GameObject.TOKEN :
					pGUI.enableReq();
					TimeClass tc = new TimeClass(30);
					pGUI.time = new Timer(1000, tc);
					pGUI.time.start();
					
					break;
				case GameObject.BESTVAL :
					pGUI.Request.setEnabled(false);
					pGUI.Fold.setEnabled(false);
					pGUI.resultlab.setText("You have the best possible hand value, wait for the result");
				}	
				if(flag == 1)
					break;
				}	   
			catch(IOException e) {
				}
				
			catch(ClassNotFoundException e2) {}
			}
		  }
		}
		class TimeClass implements ActionListener{
			int counter; 
			public TimeClass(int counter){
				this.counter = counter;
			}
			@Override
			public void actionPerformed(ActionEvent e) {
				counter--;
				if(counter >=1 ){
					pGUI.timeLab.setText(""+counter);
				}
				else{
					GameObject game = new GameObject(GameObject.HOLD, "", 0);
					sendMessage(game);
					pGUI.time.stop();
					pGUI.timeLab.setText("");
					pGUI.resultlab.setText("You have folded, wait for the decision");
				}
				
			}
			
		}

}
